package org.me.problems;

import java.util.List;

/*
  Write three functions that compute the sum of the numbers in a given
  list using a for-loop, a while-loop, and recursion.
 */
public interface ProblemOne {

    int sum(List<Integer> listOfNumbers);

}
