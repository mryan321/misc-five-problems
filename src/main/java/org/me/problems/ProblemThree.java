package org.me.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 Write a function that computes the list of the
 first 100 Fibonacci numbers. By definition, the first two numbers in
 the Fibonacci sequence are 0 and 1, and each subsequent number is
 the sum of the previous two. As an example, here are the first 10
 Fibonnaci numbers: 0, 1, 1, 2, 3, 5, 8, 13, 21, and 34.
 */
public class ProblemThree {

    //recursion?
    public List<Integer> getFibonnaciSequence(int sizeOfSequence) {
        List<Integer> fibonnaci = new ArrayList<Integer>(sizeOfSequence);
        fibonnaci.addAll(Arrays.asList(0, 1));
        for(int i=2; i<sizeOfSequence; i++) {
            int prevOne = fibonnaci.get(i-1);
            int prevTwo = fibonnaci.get(i-2);
            int nextNum = prevOne + prevTwo;
            fibonnaci.add(nextNum);
        }
        return fibonnaci;
    }

}
