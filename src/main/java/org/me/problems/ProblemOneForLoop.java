package org.me.problems;

import java.util.List;

public class ProblemOneForLoop implements ProblemOne {

    public int sum(List<Integer> listOfNumbers) {
        int sum = 0;
        for(Integer i : listOfNumbers)
            sum += i==null ? 0 : i;
        return sum;
    }

}
