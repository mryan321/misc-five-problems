package org.me.problems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 Write a function that given a list of non negative integers, arranges
 them such that they form the largest possible number. For example,
 given [50, 2, 1, 9], the largest formed number is 95021.
 */
public class ProblemFour {

    //TODO: special sort by - streams api

    public int getBiggestNumber(List<Integer> listOfNumbers) {
        //order by first digit...number 'size' not relevant
        Map<Integer, Integer> values = new HashMap<Integer, Integer>(listOfNumbers.size());
        for(Integer i : listOfNumbers)
            values.put(getKey(i), i);
        List<Integer> keys = new ArrayList<Integer>(values.keySet());
        Collections.sort(keys);
        Collections.reverse(keys); //reverses whatever order already in
        String biggestNumber = "";
        for(Integer key : keys)
            biggestNumber += values.get(key);
        return Integer.parseInt(biggestNumber);
    }

    private int getKey(int x) {
        int key = x;
        if(x > 9) {
            key = Integer.parseInt(String.valueOf(x).substring(0, 1));
        }
        return key;
    }

}
