package org.me.problems;

import java.util.List;

public class ProblemOneWhileLoop implements ProblemOne {

    public int sum(List<Integer> listOfNumbers) {
        int sum = 0;
        int i = 0;
        while(i < listOfNumbers.size()) {
            Integer value = listOfNumbers.get(i++);
            sum += value==null ? 0 : value;
        }
        return sum;
    }

}
