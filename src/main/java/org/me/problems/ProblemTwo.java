package org.me.problems;

import java.util.ArrayList;
import java.util.List;

/*
 Write a function that combines two lists by alternatingly taking
 elements. For example: given the two lists [a, b, c] and [1, 2, 3],
 the function should return [a, 1, b, 2, c, 3].
 */
public class ProblemTwo {

    //TODO: use generics when don't know? <?> <Object>
    public List combine(List firstList, List secondList) {
        int firstListSize = firstList==null ? 0 : firstList.size();
        int secondListSize = secondList==null ? 0 : secondList.size();
        List combinedList = new ArrayList<Object>(firstListSize+secondListSize);
        int maxCount = firstListSize>secondListSize ? firstListSize : secondListSize;
        for(int i=0; i<maxCount; i++) {
            if(firstListSize > i)
                combinedList.add(firstList.get(i));
            if(secondListSize > i)
                combinedList.add(secondList.get(i));
        }
        return combinedList;
    }

}
