package org.me.problems;

import java.util.ArrayList;
import java.util.List;

/*
 Write a program that outputs all possibilities to put + or - or
 nothing between the numbers 1, 2, ..., 9 (in this order) such that
 the result is always 100.
 For example: 1 + 2 + 34 – 5 + 67 – 8 + 9 = 100.
 */
public class ProblemFive {

    public String getSumsForSpecificTotal(int totalRequired) {

        List<String> allCalculationPatterns = new ArrayList<String>();
        char[] operators = { '+', '-', '|' };

        findCombinations(0, 0, operators, new ArrayList(), 4);

        return "";

        /*
             A B C
               i = 0
               j = 0
             A - loop size,
             A
             A
             A
               i = 0
               j = 1
             B [i]=[j]  A       A      A
             A          B       A      A
             A          A       B      A
             A          A       A      B
               i = 0
               j = 2
             C
             A
             A
             A

             A
             B
             A
             A
         */


//        char[] operators = { '+', '-' };
//        char[] state = { '-', '-' };
//        int[] numbers = { 0, 1, 2, 3, 4 }; //, 5, 6, 7, 8, 9 };
//        int totalOperatorsToUse = 1;
//        int maxOperatorsToUse = numbers.length - 1;

    }

    private void findCombinations(int x, int y, char[] operators, List branch, int size) {

        if(branch.size() == size) {
            System.out.println(branch);
            branch.clear();
        }

        branch.add(operators[y]);

        for(int i=x; i<size-1; i++) {
            for(int j=y; j<operators.length-1; j++) {
                findCombinations(i, ++j, operators, branch, size);
            }
            findCombinations(++i, 0, operators, branch, size);
        }
    }

    private void addCalculation(List<String> allCalculationPatterns, char[] state) {
        String calc = new String(state);
        allCalculationPatterns.add(calc);
        System.out.println(calc);
    }

}
