package org.me.problems;

import java.util.List;

public class ProblemOneRecursion implements ProblemOne {

    //will add all other elements to first number and return first number
    //when only element left
    public int sum(List<Integer> listOfNumbers) {
        if(listOfNumbers.isEmpty())
            return 0;
        int firstNumber = listOfNumbers.get(0)==null ? 0 : listOfNumbers.get(0);
        //if size = 1 then return first number
        if(listOfNumbers.size()==1)
            return firstNumber;
        //add last element to first
        int lastNumberIndex = listOfNumbers.size() - 1;
        int lastNumber = listOfNumbers.get(lastNumberIndex)==null ? 0 : listOfNumbers.get(lastNumberIndex);
        listOfNumbers.set(0, firstNumber+lastNumber);
        //new list without last element
        List<Integer> newList = listOfNumbers.subList(0, lastNumberIndex);
        return sum(newList);
    }

}
