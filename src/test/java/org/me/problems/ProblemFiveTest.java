package org.me.problems;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class ProblemFiveTest {

    ProblemFive problemFive = new ProblemFive();

    @Test
    public void testDefinedResult() {
        String expectedToContain = "1 + 2 + 34 – 5 + 67 – 8 + 9 = 100";
        String result = problemFive.getSumsForSpecificTotal(100);
        assertTrue(result.contains(expectedToContain));
    }

}