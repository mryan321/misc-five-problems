package org.me.problems;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProblemOneForLoopTest {

    private ProblemOne problemOne = new ProblemOneForLoop();

    @Test
    public void testSumWithExpectedInput() {
        List<Integer> saneInput = Arrays.asList(1, 2, 3);
        assertEquals(6, problemOne.sum(saneInput));
    }

    @Test
    public void testSumWithNull() {
        List<Integer> saneInput = Arrays.asList(1, null, 3);
        assertEquals(4, problemOne.sum(saneInput));
    }

}