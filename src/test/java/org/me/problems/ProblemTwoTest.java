package org.me.problems;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProblemTwoTest {

    ProblemTwo problemTwo = new ProblemTwo();

    @Test
    public void testDefinedResult() {
        List first = Arrays.asList("a", "b", "c");
        List second = Arrays.asList(1, 2, 3);
        List combined = problemTwo.combine(first, second);
        List expected = Arrays.asList("a", 1, "b", 2, "c", 3);
        assertEquals(6, combined.size());
        assertEquals(expected, combined);
    }

    @Test
    public void testWithBiggerFirstList() {
        List first = Arrays.asList("a", "b", "c", "d", "e", "f");
        List second = Arrays.asList(1, 2, 3);
        List combined = problemTwo.combine(first, second);
        List expected = Arrays.asList("a", 1, "b", 2, "c", 3, "d", "e", "f");
        assertEquals(9, combined.size());
        assertEquals(expected, combined);
    }

    @Test
    public void testWithBiggerSecondList() {
        List first = Arrays.asList("a", "b", "c");
        List second = Arrays.asList(1, 2, 3, 4, 5, 6);
        List combined = problemTwo.combine(first, second);
        List expected = Arrays.asList("a", 1, "b", 2, "c", 3, 4, 5, 6);
        assertEquals(9, combined.size());
        assertEquals(expected, combined);
    }

    @Test
    public void testWithNullList() {
        List first = null;
        List second = Arrays.asList(1, 2, 3);
        List combined = problemTwo.combine(first, second);
        List expected = Arrays.asList(1, 2, 3);
        assertEquals(3, combined.size());
        assertEquals(expected, combined);
    }

}