package org.me.problems;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class ProblemFourTest {

    ProblemFour problemFour = new ProblemFour();

    @Test
    public void testDefinedResult() {
        int expected = 95021;
        int result = problemFour.getBiggestNumber(Arrays.asList(50, 2, 1, 9));
        assertEquals(expected, result);
    }

}