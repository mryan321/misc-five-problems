package org.me.problems;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProblemThreeTest {

    ProblemThree problemThree = new ProblemThree();

    @Test
    public void testFirstTenFibonnaciNumbers() {
        List<Integer> expected = Arrays.asList(0, 1, 1, 2, 3, 5, 8, 13, 21, 34);
        List<Integer> result = problemThree.getFibonnaciSequence(10);
        assertEquals(expected, result);
    }

}